package Logicadifusa;

// Conjunto difuso particular : 1/2 trapecio derecha
// Forme :
// *         __________
// *        / 
// *       /  
// *______/
// ***********************
public class ConjuntoDifusoTrapecioDerecha extends ConjuntoDifuso {
    // Constructor
    public ConjuntoDifusoTrapecioDerecha(double min, double max, double finPlanoAbajo, double inicioPlanoElevado) {
        super(min, max);
        Agregar(new Punto2D(min, 0));
        Agregar(new Punto2D(finPlanoAbajo, 0));
        Agregar(new Punto2D(inicioPlanoElevado, 1));
        Agregar(new Punto2D(max, 1));
    }
}
