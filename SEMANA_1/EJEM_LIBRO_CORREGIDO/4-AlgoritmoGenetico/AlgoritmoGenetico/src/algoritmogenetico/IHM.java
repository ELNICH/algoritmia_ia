package algoritmogenetico;

// Interface genérico
public interface IHM {
    void MostrarMejorIndividuo(Individuo ind, int generation);
}
