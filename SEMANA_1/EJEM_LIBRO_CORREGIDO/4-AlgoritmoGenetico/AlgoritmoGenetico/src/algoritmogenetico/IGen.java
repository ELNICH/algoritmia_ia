package algoritmogenetico;

// Representa un gen en un genoma
public interface IGen {
    void Mutar();
}
