// prueba 1
/* 
package juego_detorreHanoi;

import java.util.Scanner;
public class Hanoi {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x;
        System.out.println("Num de discos: ");
        x = sc.nextInt();
        Hanoi(x,1,2,3);  //1:incio  2:auxiliar 3:destino                                                         
    }


    //Método Torre de Hanoi Recursivo
    public static void Hanoi(int x, int inicio,  int auxiliar, int destino){
        if(x==1){
           System.out.println("mover disco de " + inicio + " a " + destino);
        }else{
           Hanoi(x-1, inicio, destino, auxiliar);
           System.out.println("mover disco de "+ inicio + " a " + destino);
           Hanoi(x-1, auxiliar, inicio, destino);
        }
    }
}
*/
//====================================================================================
//====================================================================================

// prueba 2
/* 
package juego_detorreHanoi;

import java.util.Scanner;

public class Hanoi {
    private static final int INICIO = 1;
    private static final int AUXILIAR = 2;
    private static final int DESTINO = 3;
    private static final String MOVIMIENTO = "mover disco de %d a %d";

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Número de discos: ");
            int numDiscos = sc.nextInt();
            resolverHanoi(numDiscos, INICIO, AUXILIAR, DESTINO);
        }
    }

    // Método Torre de Hanoi Recursivo
    public static void resolverHanoi(int numDiscos, int inicio, int auxiliar, int destino) {
        if (numDiscos == 1) {
            System.out.println(String.format(MOVIMIENTO, inicio, destino));
        } else {
            resolverHanoi(numDiscos - 1, inicio, destino, auxiliar);
            System.out.println(String.format(MOVIMIENTO, inicio, destino));
            resolverHanoi(numDiscos - 1, auxiliar, inicio, destino);
        }
    }
}
*/

//============================================================================================
//            A L G O R I T M O    M E J O R A D O   T O R R E   D E   H A N O I
//============================================================================================
// prueba 3  (optimizado)

package juego_detorreHanoi;

import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;

public class Hanoi {
    private static final int INICIO = 1;
    private static final int AUXILIAR = 2;
    private static final int DESTINO = 3;
    private static final String MOVIMIENTO = "mover disco de %d a %d";
    private static Map<String, String> cache = new HashMap<>();

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Número de discos: ");
            int numDiscos = sc.nextInt();
            resolverHanoi(numDiscos, INICIO, AUXILIAR, DESTINO);
        }
    }

    // Método Torre de Hanoi Recursivo con memorización
    public static void resolverHanoi(int numDiscos, int inicio, int auxiliar, int destino) {
        if (numDiscos == 1) {
            System.out.println(String.format(MOVIMIENTO, inicio, destino));
        } else {
            String key = numDiscos + "-" + inicio + "-" + auxiliar + "-" + destino;
            if (!cache.containsKey(key)) {
                resolverHanoi(numDiscos - 1, inicio, destino, auxiliar);
                cache.put(key, String.format(MOVIMIENTO, inicio, destino));
                resolverHanoi(numDiscos - 1, auxiliar, inicio, destino);
            }
            System.out.println(cache.get(key));
        }
    }
}
