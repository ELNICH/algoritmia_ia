class Ferreteria:
    def __init__(self):
        self.clientes = {}

    def registrar_cliente(self, nombre, direccion):
        self.clientes[nombre] = {
            'direccion': direccion,
            'pedidos': []
        }

    def realizar_pedido(self, cliente, producto):
        if cliente in self.clientes:
            confirmacion = input(f"Realizando pedido de {producto} para {cliente}. ¿Confirmar? (si/no): ")
            if confirmacion.lower() == 'si':
                self.clientes[cliente]['pedidos'].append(producto)
                print("Pedido registrado exitosamente.")
            else:
                print("Pedido cancelado.")
        else:
            print("Cliente no registrado.")

    def anular_pedido(self, cliente):
        if cliente in self.clientes:
            if self.clientes[cliente]['pedidos']:
                print(f"Pedidos de {cliente}: {self.clientes[cliente]['pedidos']}")
                self.anular_pedido_recursivo(cliente)
            else:
                print("El cliente no tiene pedidos registrados.")
        else:
            print("Cliente no registrado.")

    def anular_pedido_recursivo(self, cliente):
        pedido_a_anular = input("Ingrese el pedido que desea anular (o 'salir' para salir): ")
        if pedido_a_anular == 'salir':
            return
        if pedido_a_anular in self.clientes[cliente]['pedidos']:
            self.clientes[cliente]['pedidos'].remove(pedido_a_anular)
            print("Pedido anulado.")
            self.anular_pedido_recursivo(cliente)
        else:
            print("Pedido no encontrado.")
            self.anular_pedido_recursivo(cliente)

def main():
    ferreteria = Ferreteria()

    while True:

        print("")
        print("==================================================================")
        print("------¡¡Hola bienvenido a la Ferreteria El Tornillo Feliz!!-------")
        print("==================================================================")
        print("")
        
        print(" Que desea realizar")
        print("1. Registrar Cliente")
        print("2. Realizar Pedido")
        print("3. Anular Pedido")
        print("4. Salir")
        opcion = input("Seleccione una opción: ")

        if opcion == '1':
            nombre = input("Ingrese el nombre del cliente: ")
            direccion = input("Ingrese la dirección del cliente: ")
            ferreteria.registrar_cliente(nombre, direccion)
            print("Cliente registrado exitosamente.")

        elif opcion == '2':
            nombre = input("Ingrese el nombre del cliente: ")
            producto = input("Ingrese el producto a pedir: ")
            ferreteria.realizar_pedido(nombre, producto)

        elif opcion == '3':
            nombre = input("Ingrese el nombre del cliente: ")
            ferreteria.anular_pedido(nombre)

        elif opcion == '4':
            print("¡Hasta luego!")
            break

        else:
            print("Opción no válida. Por favor, seleccione una opción válida.")

if __name__ == "__main__":
    main()
