import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

data_path = 'trfinal_excel.csv'
data = pd.read_csv(data_path, sep=';', encoding='utf-8')
X = data.drop('Satisfacción del Cliente', axis=1)
y = data['Satisfacción del Cliente']

X = pd.get_dummies(X)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

random_forest = RandomForestClassifier(n_estimators=100, random_state=42)  # Puedes ajustar el número de árboles (n_estimators)

random_forest.fit(X_train, y_train)

y_pred = random_forest.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print(f'Precisión del modelo Random Forest: {accuracy}')

def entropy(y):
    if len(y) == 0 or y.isnull().all():
        return 0
    p1 = (y == 1).sum() / len(y)
    p0 = 1 - p1
    if p0 == 0 or p1 == 0:
        return 0
    return -p0 * np.log2(p0) - p1 * np.log2(p1)

def information_gain(X, y, attribute):
    ent_before = entropy(y)
    ent_after = 0
    for value in X[attribute].unique():
        subset_y = y[X[attribute] == value]
        ent_after += len(subset_y) / len(y) * entropy(subset_y)
    return ent_before - ent_after

def id3(X, y, max_depth=None):
    if max_depth is not None and max_depth <= 0:
        return y.mode().iloc[0]  
    best_attribute = None
    best_gain = -1
    for attribute in X.columns:
        gain = information_gain(X, y, attribute)
        if gain > best_gain:
            best_gain = gain
            best_attribute = attribute
    
    if best_gain == 0:
        return y.mode().iloc[0]  
    tree = {best_attribute: {}}
    for value in X[best_attribute].unique():
        X_subset = X[X[best_attribute] == value].drop(columns=best_attribute)
        y_subset = y[X[best_attribute] == value]
        tree[best_attribute][value] = id3(X_subset, y_subset, max_depth=max_depth - 1 if max_depth is not None else None)
    
    return tree

max_depth_id3 = 10  
tree_id3 = id3(X_train, y_train, max_depth=max_depth_id3)

def predict(tree, X):
    predictions = []
    for i in range(len(X)):
        node = tree
        while isinstance(node, dict):
            attribute = list(node.keys())[0]
            value = X.iloc[i][attribute]
            node = node[attribute].get(value)  
        predictions.append(node)
    return predictions

y_pred_id3 = predict(tree_id3, X_test)

accuracy_id3 = accuracy_score(y_test, y_pred_id3)
print(f'Precisión ID3 actualizada: {accuracy_id3}')

def print_tree(tree, depth=0):
    if isinstance(tree, dict):
        for attribute, subtree in tree.items():
            print("  " * depth + f"- {attribute}")
            for value, subsubtree in subtree.items():
                print("  " * (depth + 1) + f"- {value}")
                print_tree(subsubtree, depth + 2)
    elif tree == 'Sí' or tree == 'No':
        print("  " * depth + f"- Satisfacción del Cliente: {tree}")

print("\nEstructura del Árbol ID3 actualizado:")
print_tree(tree_id3)



