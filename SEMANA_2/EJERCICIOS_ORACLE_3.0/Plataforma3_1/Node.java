package Plataforma3_1;

public class Node {
        int data;
        Node left;
        Node right;

    public Node(int data) {
        this.data = data;
        left = null;
        right = null;
    }
    public void setLeft(Node node) {
        if (left == null)
        left = node;
    }
    public void setRight(Node node) {
        if (right == null)
        right = node;
    }
    public Node getLeft() {
        return left;
    }
    public Node getRight() {
        return right;
    }
    public int getData() {
        return data;
    }
    public void setData(int data) {
        this.data = data;
    }

    public static void main(String[] args) {
        Node abcNode = new Node(10);
        System.out.println("node: " + abcNode.data);
    }
}