package Plataforma3_3;

public class Node {
    int data;
    Node left;
    Node right;

    public Node(int data) {
        this.data = data;
        left = null;
        right = null;
    }

    public void setLeft(Node node) {
        if (left == null)
            left = node;
    }

    public void setRight(Node node) {
        if (right == null)
            right = node;
    }

    public static void main(String[] args) {
        Node abcNode = new Node(10);
        System.out.println("node: " + abcNode.data);

        System.out.println("Preorder:");
        abcNode.printPreorder(abcNode);

        System.out.println("Postorder:");
        abcNode.printPostorder(abcNode);

        System.out.println("Inorder:");
        abcNode.printInorder(abcNode);
    }

    void printPreorder(Node node) {
        if (node == null)
            return;

        System.out.println(node.data + " ");
        printPreorder(node.left);
        printPreorder(node.right);
    }

    void printPostorder(Node node) {
        if (node == null)
            return;

        printPostorder(node.left);
        printPostorder(node.right);
        System.out.println(node.data + " ");
    }

    void printInorder(Node node) {
        if (node == null)
            return;

        printInorder(node.left);
        System.out.println(node.data + " ");
        printInorder(node.right);
    }
}
