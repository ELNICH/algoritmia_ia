package Plataforma3_2;

        public class O2_Factorial {
            public static int factorial(int n) {
                if (n == 0 || n == 1) {
                    return 1;
                }
                return n * factorial(n - 1);
            }
        
            public static void main(String[] args) {
                int numero = 5;
                int resultado = factorial(numero);
                System.out.println("Factorial de " + numero + " es: " + resultado);
            }
        }
    