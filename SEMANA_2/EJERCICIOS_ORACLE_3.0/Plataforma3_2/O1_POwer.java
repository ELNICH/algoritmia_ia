package Plataforma3_2;
public class O1_POwer {
     int power(int a, int n)
    {
        int result = 1;
        for(int i=0;i<n;i++) {
            result *=a;
        }
        return result;
    } 
    
    int power1(int a, int n)
    {
        if (n == 0) {
            return 1;
        }
        else {
            return a * power1(a, n - 1);
        }
    }
    public static void main(String[] args) {
        int a = 2;
        int n =3;
        O1_POwer obj = new O1_POwer();
        System.out.println("a: " + a + " n: " +n);
        System.out.println("Power: " + obj.power(a, n));
        System.out.println("Power1: " + obj.power1(a, n));

    }
}
    

