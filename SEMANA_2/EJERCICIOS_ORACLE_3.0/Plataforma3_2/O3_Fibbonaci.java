package Plataforma3_2;

public class O3_Fibbonaci {
    public static int Fibonacci(int x) {
        if (x < 2) {
            return 1;
        } else {
            return Fibonacci(x - 1) + Fibonacci(x - 2);
        }
    }

    public static void main(String[] args) {
        int n = 10;
        System.out.println("Fibonacci : " + n + " serie : ");
        for (int i = 0; i < n; i++) {
            System.out.print(Fibonacci(i) + " ");
        }
    }
}

