package fibo;

public class fibonacci {
    private long cont = 0;

    // recursivo
    public long calcularFibonacciR(long n) {
        System.out.println(++cont);
        if (n <= 1) {
            return n;
        }
        return calcularFibonacciR(n - 1) + calcularFibonacciR(n - 2);
    }

    // iterativo
    public long calcularFibonacciI(long n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        long fn = 1, f1 = 1, f2 = 0, i;
        for (i = 1; i < n; i++) {
            fn = f1 + f2;
            f2 = f1;
            f1 = fn;
        }

        return fn;
    }

    public static void main(String[] args) {
        fibonacci fib = new fibonacci();
        long n = 10;
        System.out.println("Fibonacci recursivo de " + n + ": " + fib.calcularFibonacciR(n));
        System.out.println("Fibonacci iterativo de " + n + ": " + fib.calcularFibonacciI(n));
    }
}