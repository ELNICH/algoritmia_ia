package fibo;
import java.util.Scanner;

public class FibonacciMain {

   
    public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa Un Numero Para Calcular La Serie De Fibonacci:\n");
        long num = sc.nextLong();
        
        fibonacci fb = new fibonacci();
        System.out.println("[ITERATIVO] Fibonacci creado:  "+ fb.calcularFibonacciI(num)+"\n");
        System.out.println("[RECURSIVO] Fibonacci creado:  "+ fb.calcularFibonacciR(num)+"\n");
         
    }
    
}
