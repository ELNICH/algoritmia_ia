package vista;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import arbol.Arbol;

public class principal1 extends javax.swing.JFrame {

    Arbol arbol;

    public principal1() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        // ... (otros campos de texto)
        jButton1 = new javax.swing.JButton();
        // ... (otros botones)
        jTextArea1 = new javax.swing.JTextArea();
        // ... (otros componentes)

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout()); // Establece el BorderLayout en el JFrame

        // Panel superior con los campos de texto
        JPanel textFieldsPanel = new JPanel();
        textFieldsPanel.add(jTextField1);
        textFieldsPanel.add(jTextField2);
        // ... (otros campos de texto)
        add(textFieldsPanel, BorderLayout.NORTH);

        // Panel central con los campos de texto en cuadrícula
        JPanel gridPanel = new JPanel(new GridLayout(2, 4));
        gridPanel.add(jTextField4);
        gridPanel.add(jTextField5);
        // ... (otros campos de texto jTextField6 a jTextField11)
        add(gridPanel, BorderLayout.CENTER);

        // Panel izquierdo con los botones
        JPanel buttonsPanel = new JPanel(new GridLayout(10, 1));
        buttonsPanel.add(jButton1);
        // ... (otros botones)
        add(buttonsPanel, BorderLayout.WEST);

        // Panel derecho con el área de texto
        add(new JScrollPane(jTextArea1), BorderLayout.EAST);

        // ... (resto del código)

        pack();
    }

    // ... (resto del código)

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // ... (resto del código)

    public void cargarArbol() {
        // ... (código para cargar el árbol)
    }

    private javax.swing.JButton jButton1;
    // ... (otros botones)
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // ... (otros campos de texto)
    private javax.swing.JTextArea jTextArea1;
    // ... (otros componentes)
}
